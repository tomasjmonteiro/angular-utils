# Exsurge Renderer for Angular

Angular component that can be used to render gregorian chant in the GABC format, either in SVG or PNG.

This component uses a fork created by Benjamin Bloomfield from the exsurge component by Fr. Matthew Spencer, OSJ.

Currently, the exsurge used by this component is at commit 95456f1e.

## Usage

This project was built and tested with the following configuration:

Angular CLI: 7.3.6

Node: 10.15.1

Angular: 7.2.10


To use the component, install it with `npm install exsurge-angular`. Then, on the component, just import it invoking 
`import {ExsurgeRendererModule} from 'exsurge-angular'`.

On the html template, just use the tag `<lib-exsurge-renderer/>` and pass the source to render as below:

```html

<h1>EXAMPLES</h1>
  <div>
    <h2>Render in canvas</h2>
    <textarea [(ngModel)]="canvasTextAreaValue" rows="25"></textarea>
    <lib-exsurge-renderer source="{{canvasTextAreaValue}}" [isRenderInCanvas]="true"></lib-exsurge-renderer>
    <p></p>
  </div>
  <div>
    <h2>Render in SVG</h2>
    <textarea [(ngModel)]="svgTextAreaValue" rows="25"></textarea>
    <lib-exsurge-renderer source="{{svgTextAreaValue}}" [isRenderInCanvas]="false"></lib-exsurge-renderer>
  </div>


```

## External sources:

* Fr. Matthew Spencer's repository: https://github.com/frmatthew/exsurge
* Benjamin Bloomfield's repository: https://github.com/bbloomf/exsurge
* Gregorio project: https://github.com/gregorio-project/gregorio
