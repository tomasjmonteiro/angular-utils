import {AfterViewInit, Component, HostListener, Input, OnChanges, SimpleChanges} from '@angular/core';
const exsurge = require('../ext/exsurge/exsurge');
import {GabcConstants} from './GabcConstants';
import {GabcChant} from './GabcChant';
import {Guid} from 'guid-typescript';

@Component({
  selector: 'lib-exsurge-renderer',
  templateUrl: './exsurge-renderer.component.html',
  styleUrls: ['./exsurge-renderer.component.css']
})
export class ExsurgeRendererComponent implements AfterViewInit, OnChanges {


  // tslint:disable-next-line:no-input-rename
  @Input('source') rawSource: string;

  @Input() isRenderInCanvas: boolean;

  public id: Guid;
  private ctxt: any;
  private score: any;
  private chantCanvasContainer: HTMLElement;
  private chantSvgContainer: HTMLElement;
  private processedSource: GabcChant;


  constructor() {
    this.id = Guid.create();
  }

  /**
   * Update the score mappings with the source, then invokes the rendering.
   */
  private renderGregorianChant() {

    // process the raw input to make it compatible with exsurge
    if (this.processedSource) {
      this.processedSource.updateGabcTranslation(this.rawSource);
    } else {
      this.processedSource = new GabcChant(this.rawSource);
    }

    // (re)create the chant mappings
    if (this.score) {
      this.updateScoreWithHeaderFields();
      exsurge.Gabc.updateMappingsFromSource(this.ctxt, this.score.mappings, this.processedSource.gabcChantDescription);
      this.score.updateNotations(this.ctxt);

    } else {
      const mappings = exsurge.Gabc.createMappingsFromSource(this.ctxt, this.processedSource.gabcChantDescription);
      this.score = new exsurge.ChantScore(this.ctxt, mappings, this.getInitialStyleSettingFromHeader());
    }

    this.layoutChantAsync();

  }

  /**
   * Gets the initial-style parameter from the header
   */
  private getInitialStyleSettingFromHeader() {
    if (this.processedSource.header.has(GabcConstants.INITIAL_STYLE)) {
      return this.processedSource.header.get(GabcConstants.INITIAL_STYLE)[0] === '1';
    } else {
      return true;
    }
  }

  private updateScoreWithHeaderFields() {
    // fixme: behaviour with drop caps:
    //   when {initial-style:0;annotation: something;} => update gabc => annotations appear (were hidden before, despite being set in gabc).
    //   when {initial-style} is changed from 0 to 1 and back to 0, the drop cap does not disappear.

    // 1. update annotations
    if (this.processedSource.header.has(GabcConstants.ANNOTATION)) {
      const headerAnnotations = this.processedSource.header.get(GabcConstants.ANNOTATION).map(value => {
        if (value.trim().length > 0) {
          return '%' + value + '%';
        } else {
          return '';
        }
      });

      this.score.annotation = new exsurge.Annotations(this.ctxt, ...headerAnnotations);
    }

    // 2. update other fields here
    this.score.useDropCap = this.getInitialStyleSettingFromHeader(); // todo is this how it is supposed?
  }

  /**
   * Build the chant layout asynchronously.
   */
  private layoutChantAsync() {
    this.score.performLayoutAsync(this.ctxt, this.afterLayout);

  }

  /**
   * Callback to be invoked after the layout, runs the final step of the layout and
   * draws the image or the svg according to the input of the component.
   */
  private afterLayout = () => {

    if (this.isRenderInCanvas === true) {
      this.score.layoutChantLines(this.ctxt, this.chantCanvasContainer.clientWidth, () => {
        this.drawCanvas();
      });

    } else {
      this.score.layoutChantLines(this.ctxt, this.chantSvgContainer.clientWidth, () => {
        while (this.chantSvgContainer.firstChild) {
          this.chantSvgContainer.removeChild(this.chantSvgContainer.firstChild);
        }
        this.drawSvg();
      });

    }
  };

  private drawCanvas() {
    this.score.draw(this.ctxt);
  }

  private drawSvg() {
    this.chantSvgContainer.appendChild(this.score.createSvgNode(this.ctxt));
  }

  private setupExsurge() {
    this.ctxt = new exsurge.ChantContext();
    this.ctxt.setFont('\'Crimson Text\', serif', 19.2);
    this.ctxt.dropCapTextFont = this.ctxt.lyricTextFont;
    this.ctxt.annotationTextFont = this.ctxt.lyricTextFont;
    this.ctxt.textMeasuringStrategy = exsurge.TextMeasuringStrategy.Canvas;
    this.ctxt.minLyricWordSpacing = this.ctxt.hyphenWidth * 0.7;
    this.chantSvgContainer = document.getElementById('chant-svg-container-' + this.id.toString());
    this.chantCanvasContainer = document.getElementById('chant-canvas-container-' + this.id.toString());
    if (this.isRenderInCanvas === true) {
      this.chantCanvasContainer.appendChild(this.ctxt.canvas);
    }
  }

  /**
   * Only invoke Exsurge after Angular has completed initialization of the component's view,
   * to have access to the proper template element id's on document.getElementById()
   */
  ngAfterViewInit(): void {
    this.setupExsurge();
    this.renderGregorianChant();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.score) {
      this.renderGregorianChant();
    } else {
      console.log('onChange triggered before init. This is the first load of page');
    }
  }

  @HostListener('window:resize', [])
  onResize() {
    this.layoutChantAsync();
  }
}
