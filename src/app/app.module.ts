import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {OsmdRendererModule} from 'osmd-angular';
import {FormsModule} from '@angular/forms';
import {ExsurgeRendererModule} from 'exsurge-angular';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ExsurgeRendererModule,
    OsmdRendererModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
