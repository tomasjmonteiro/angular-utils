export class GabcConstants {

  // Definition of gabc header tags
  public static readonly HEADER_SEPARATOR = '%%';
  public static readonly NAME = 'name';
  public static readonly GABC_COPYRIGHT = 'gabc-copyright';
  public static readonly SCORE_COPYRIGHT = 'score-copyright';
  public static readonly OFFICE_PART = 'office-part';
  public static readonly OCCASION = 'occasion';
  public static readonly METER = 'meter';
  public static readonly COMMENTARY = 'commentary';
  public static readonly ARRANGER = 'arranger';
  public static readonly AUTHOR = 'author';
  public static readonly DATE = 'date';
  public static readonly MANUSCRIPT = 'manuscript';
  public static readonly MANUSCRIPT_REFERENCE = 'manuscript-reference';
  public static readonly MANUSCRIPT_STORAGE_PLACE = 'manuscript-storage-place';
  public static readonly BOOK = 'book';
  public static readonly LANGUAGE = 'language';
  public static readonly TRANSCRIBER = 'transcriber';
  public static readonly TRANSCRIPTION_DATE = 'transcription-date';
  public static readonly MODE = 'mode';
  public static readonly INITIAL_STYLE = 'initial-style';
  public static readonly USER_NOTES = 'user-notes';
  public static readonly ANNOTATION = 'annotation';

  // Regular expressions
  public static readonly REGEX_ITALIC_TAG: RegExp = new RegExp('<\\s*\\/*i>', 'g');
  public static readonly REGEX_BOLD_TAG: RegExp = new RegExp('<\\s*\\/*b>', 'g');
  public static readonly REGEX_BARRED_R: RegExp = new RegExp('<sp>R/</sp>', 'g');
  public static readonly REGEX_BARRED_V: RegExp = new RegExp('<sp>V/</sp>', 'g');
  public static readonly REGEX_BARRED_A: RegExp = new RegExp('<sp>A/</sp>', 'g');
}
