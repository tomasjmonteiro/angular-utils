import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExsurgeRendererComponent } from './exsurge-renderer.component';

describe('ExsurgeRendererComponent', () => {
  let component: ExsurgeRendererComponent;
  let fixture: ComponentFixture<ExsurgeRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExsurgeRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExsurgeRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
