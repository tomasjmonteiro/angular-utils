import { TestBed } from '@angular/core/testing';

import { ExsurgeRendererService } from './exsurge-renderer.service';

describe('ExsurgeRendererService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExsurgeRendererService = TestBed.get(ExsurgeRendererService);
    expect(service).toBeTruthy();
  });
});
