import {GabcConstants} from './GabcConstants';

export class GabcChant {

  /**
   * The chant headers, compliant with gregorio-project definition.
   */
  public header: Map<string, Array<string>>;

  /**
   * The chant source.
   */
  public gabcChantDescription: string;

  /**
   * Initializes a new gabc chant object.
   * @param rawSource the gabc source compliant with gregorio-project definition.
   */
  constructor(rawSource: string) {
    this.header = new Map();
    this.updateGabcTranslation(rawSource);
  }

  /**
   * Process the gabc source for usage in exsurge.
   *
   * @param rawSource the gabc source compliant with gregorio-project definition.
   */
  public updateGabcTranslation(rawSource: string) {

    const splitSource = rawSource.split(GabcConstants.HEADER_SEPARATOR);

    if ((splitSource.length === 1)) {
      // No headers present, assign the source directly
      this.gabcChantDescription = splitSource[0];
    } else {

      // Start the map from scratch, get each header, that is separated by a semi colon, and add it to map.
      this.header.clear();
      splitSource[0].split(';').forEach((entry: string) => {

        const entryParts = entry.split(':');

        if (entryParts.length > 1) {
          const key = entryParts[0].replace('\n', '');
          if (this.header.has(key)) {
            this.header.get(key).push(entryParts[1]);
          } else {
            this.header.set(key, [entryParts[1]]);
          }
        }

      });

      splitSource[1] = splitSource[1].replace(GabcConstants.REGEX_ITALIC_TAG, '_')
        .replace(GabcConstants.REGEX_BOLD_TAG, '*')
        .replace(GabcConstants.REGEX_BARRED_A, 'A/.')
        .replace(GabcConstants.REGEX_BARRED_R, 'R/.')
        .replace(GabcConstants.REGEX_BARRED_V, 'V/.');

      this.gabcChantDescription = splitSource[1];
    }
  }
}
