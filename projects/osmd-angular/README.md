# OpenSheetMusicDisplay Renderer for Angular

Angular component for OpenSheetMusicDisplay. Right now only the source is passed. 

## Usage

This project was built and tested with the following configuration:

Angular CLI: 7.3.6

Node: 10.15.1

Angular: 7.2.10


To use the component, install it with `npm install osmd-angular`. Then, on the component, just import it invoking 
`import {OsmdRendererModule} from 'osmd-angular'`.

On the html template, just use the tag `<lib-osmd-renderer/>` and pass the URL or XML source to render as below:

```html

<div>
  <h1>Open Sheet Music Display Demo</h1>
  <label for="music-xml-source">URL or XML contents:</label>
  <textarea id="music-xml-source" style="width: 100%;" [(ngModel)]="musicXml" rows="10"></textarea>
  <lib-osmd-renderer source="{{musicXml}}"></lib-osmd-renderer>
</div>



```

## External sources:

* https://github.com/opensheetmusicdisplay/opensheetmusicdisplay
