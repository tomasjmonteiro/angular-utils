# Angular Utils

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.

## Development Setup

Initialize the submodule for exsurge: run `git submodule init && git submodule update`.

## Upgrading Exsurge dependency

Whenever the submodule is pulled to a later commit, the dependency in projects/exsurge-angular/src/ext/exsurge must be updated as well.

To do that, the submodule must be built as follows:

```
$ cd external_repos/exsurge
$ npm install
$ npm run build-dev

```

Copy the js files from external_repos/exsurge/dist into projects/exsurge-angular/src/ext/exsurge and commit them. DO NOT COMMIT any files under the external_repos/ folder!

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Testing

It is recommended that a blank application is created to test that the packages can be properly installed from npm.

## Updating and publishing a new version.

To build the projects there are several options available on the parent project:

  *  `npm run build_lib_exsurge`: builds the exsurge-angular project;
  *  `npm run pack_lib_exsurge`: builds the exsurge-angular project and prepares it for publishing;
  *  `npm run build_lib_osmd`: builds the osmd-angular project;
  *  `npm run pack_lib_osmd`: builds the osmd-angular project and prepares it for publishing;
  *  `npm run package_all`: builds the exsurge-angular and osmd-angular projects and prepares them for publishing.


When the library is stable and all the changes planned for the new release are completed, go to the project `package.json` file and 
increase the version number according to semver (https://semver.org/) rules, then commit the file.

Run one of the options above that prepares for publishing. this creates a .tgz file (for each of the projects, if the package_all is invoked), under `dist/{project-name}`.

run `npm publish dist/{project-name}/{package-name}-x.y.z.tgz` replacing the folder and file names with the actual ones.


## External references

### _The Angular Library Series_ by Todd Palmer

NOTE: These parts are also available on docs/ folder.

Part 1 - https://blog.angularindepth.com/creating-a-library-in-angular-6-87799552e7e5

Part 2 - https://blog.angularindepth.com/creating-a-library-in-angular-6-part-2-6e2bc1e14121

Part 3 - https://blog.angularindepth.com/the-angular-library-series-publishing-ce24bb673275
