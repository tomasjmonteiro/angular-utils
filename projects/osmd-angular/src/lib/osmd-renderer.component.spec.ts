import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OsmdRendererComponent } from './osmd-renderer.component';

describe('OsmdRendererComponent', () => {
  let component: OsmdRendererComponent;
  let fixture: ComponentFixture<OsmdRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OsmdRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OsmdRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
