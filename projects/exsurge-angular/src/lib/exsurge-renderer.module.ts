import {NgModule} from '@angular/core';
import {ExsurgeRendererComponent} from './exsurge-renderer.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [ExsurgeRendererComponent],
  imports: [
    CommonModule
  ],
  exports: [ExsurgeRendererComponent]
})
export class ExsurgeRendererModule {
}
