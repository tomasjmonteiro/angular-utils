import { TestBed } from '@angular/core/testing';

import { OsmdRendererService } from './osmd-renderer.service';

describe('OsmdRendererService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OsmdRendererService = TestBed.get(OsmdRendererService);
    expect(service).toBeTruthy();
  });
});
