import { NgModule } from '@angular/core';
import { OsmdRendererComponent } from './osmd-renderer.component';

@NgModule({
  declarations: [OsmdRendererComponent],
  imports: [
  ],
  exports: [OsmdRendererComponent]
})
export class OsmdRendererModule { }
